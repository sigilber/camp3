//Created by : Francois Genois
#pragma once
#include <thread>
#include <vector>
#include <mutex>
#include <memory>
#include <chrono>
#include "MDData.h"
#include "Semaphore.h"
#include "AudioPlayer.h"

class MDModule
{
public:
    using size_type = std::vector<MDData>::size_type;

private:
    std::thread* threadModule;
    bool continueExecution = false;

    const size_type groupSize;
    std::vector<MDData> bufferMDData;
    size_type bufferElementCount;
    size_type begin;
    size_type end;
    std::mutex mutexBuffer;
    Semaphore thingsToDo;
    std::chrono::system_clock::time_point lastCommandSent;
    int deltaTimeCommands;

    void ThreadMDModule();
    size_type GetNextIndex(const size_type currentIndex, const size_type increment = 1);
    size_type GetPreviousIndex(const size_type currentIndex, const size_type decrement = 1);
    bool GetMDDataGroup(MDData* outData, const int size);
    void RemoveLastElement();
    MDData::MovementStrenght GetXMovement(const MDData* data, const int size) const;
    MDData::MovementStrenght GetYMovement(const MDData* data, const int size) const;

public:
    MDModule(std::shared_ptr<AudioPlayer> audioPlayer, size_type nbGroup = 5, size_type sizeGroup = 3, int millisecondsDeltaCommands = 1000);
    //The audio player must be initiated prior to calling this function
    // -Audio files must be added
    void Init();
    void Abort();

    //Adds a MMData to the list if a spot is available.
    // Otherwise we are dropping the data.
    void AddMDData(const MDData& data);

    std::shared_ptr<AudioPlayer> audioPlayer;
};