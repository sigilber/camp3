//Created by : Simon Gilbert
//Edited by : François Genois
#include <iostream>
#include <thread>

#include <condition_variable>
#include <mutex>

#include "camp3Manager.h"
#include "CaMP3Analyser.h"

using std::thread;
using std::mutex;
using std::condition_variable;

using namespace std::chrono;

camp3Manager::camp3Manager(int fps_)
    : captureHelper{}
    , m{std::make_shared<AudioPlayer>()}
    , fps{fps_}
{
    m.audioPlayer->AddFiles("./songs/");
    m.Init();
}

string in = "";
void watch()
{
    std::cin>>in;
}

void camp3Manager::detecte()
{
    thread t(watch);
    cv::Mat image;
    captureHelper.GetImage(image);
    CaMP3Analyser analyser(image);
    analyser.Init();
    std::unique_lock<mutex> lock(cv_m);
    microseconds_type ideal{ 1000000 / fps };

    while(in == "")
    {
        auto start = high_resolution_clock::now();

        captureHelper.GetImage(image);
        auto mdData = analyser.AnalyseNextImage(image);
        //std::cout << "the stuff " << mdData.xValue << " " << mdData.yValue << std::endl;
        m.AddMDData(mdData);

        auto end = high_resolution_clock::now();
        std::cout<<duration_cast<milliseconds>(end - start).count()<<"\n";
        microseconds_type duree =  duration_cast<microseconds>(end - start);
        cv.wait_for(lock, ideal - duree);
       // std::this_thread::sleep_for(ideal-duree);
    }
    //c.saveAsPPM("asd.ppm", &c.save_img);
    t.join();
}
