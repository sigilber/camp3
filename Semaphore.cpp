//Created by : Francois Genois
#include "Semaphore.h"

using std::unique_lock;
using std::mutex;

void Semaphore::Up()
{
    unique_lock<mutex> lck(mtx);
    ++count;
    cv.notify_one();
}

void Semaphore::Down()
{
    unique_lock<mutex> lck(mtx);

    while(count == 0){
        cv.wait(lck);
    }
    --count;
}