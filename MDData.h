//Created by : Francois Genois
#pragma once
#include <limits>

class MDData
{
public:
    using precision_type = int;
    enum MovementStrenght { POSITIVE, NEGATIVE, NONE };

public://TODO: privatise
    static constexpr precision_type thresholdLowX = 50;
    static constexpr precision_type thresholdLowY = 50;
    static constexpr precision_type thresholdHiX = std::numeric_limits<precision_type>::max();
    static constexpr precision_type thresholdHiY = std::numeric_limits<precision_type>::max();

    precision_type xValue;
    precision_type yValue;

    MovementStrenght GetMovementStrenght(precision_type threshold,
                                            precision_type thresholdHi,
                                            precision_type value) const;

public:
    MDData();
    MDData(precision_type _xValue, precision_type _yValue);

    void SetXValue(precision_type _xValue);
    void SetYValue(precision_type _yValue);

    MovementStrenght GetHorizontalMovement() const;
    MovementStrenght GetVerticalMovement() const;
};

inline void MDData::SetXValue(precision_type _xValue)
{
    xValue = _xValue;
}

inline void MDData::SetYValue(precision_type _yValue)
{
    yValue = _yValue;
}