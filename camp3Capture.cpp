//Created by : Simon Gilbert
#include "camp3Capture.h"

    camp3Capture::camp3Capture(){
        VideoCapture cap(0);
        capture();
    }

    camp3Capture::camp3Capture(VideoCapture c):cap{c}{
         cap >> save_img;
         ancienneVariationX = 0;
         ancienneVariationY = 0;
    }

    camp3Capture::camp3Capture(Mat m):save_img{m}{

    }

     camp3Capture::camp3Capture(string m){
        save_img = imread(m, CV_LOAD_IMAGE_COLOR);
    }

    camp3Capture::~camp3Capture(){
        cap.release();
    }


    const Mat* camp3Capture::detecterContours(){
        //cvtColor(save_img, contours, cv::COLOR_BGR2GRAY);
        variationHorizontale(contours);
        variationVerticale(contours);
        return &contours;
    }

    void camp3Capture::contourVertical(Mat& m){
        It img = save_img.begin<VT>(), img_end = save_img.end<VT>(), img_next = img, contour = m.begin<VT>(), img_begin=img;
        img_next+=save_img.rows;
        for(int i = 0; i<save_img.cols;++i){
            for(int j = 0; j<save_img.rows&&img_next!=img_end;++j){
                *contour = *img_next - *img;
                img+=save_img.cols;
                img_next+=save_img.cols;
                contour+=save_img.cols;
            }
            img = img_begin;
            img+=i;
            contour = m.begin<VT>();
            contour+=i;
            img_next=img;img_next++;
        }
    }

    void camp3Capture::contourHorizontal(Mat& m){
        It img = save_img.begin<VT>(), img_end = save_img.end<VT>(), img_next = img, contour = m.begin<VT>();
        ++img_next;
        for(int i = 0; i<save_img.rows*save_img.cols&&img_next!=img_end;++i){
                *contour = *img_next - *img;
                ++img;++img_next;++contour;
        }
        *contour=VT{0, 0, 0};
    }

     int camp3Capture::variationHorizontale(Mat& m){
        int res=0;
        int cols=0;

        for(int i = 0; i<m.rows;++i){
            for(int j = 0; j<m.cols-1;++j){
                VT next_px = m.at<Vec3b>(i, j+1);
                VT px = m.at<Vec3b>(i, j);

                int contraste{next_px[0]-px[0]+next_px[1]-px[1]+next_px[2]-px[2]};//(next_px[0]-px[0])+(next_px[1]-px[1])+(next_px[2]-px[2]))

                if(contraste>392)//392 = 3 X 128, utilisé pour faire la moyenne sans diviser.
                    res+=j;
                ++cols;
            }
        }
        return res;
    }

     MDData camp3Capture::variationBidirectionnelle(){

        int resX=0;
        int resY=0;

          for(int i = 0; i<save_img.rows-1;++i){
            for(int j = 0; j<save_img.cols-1;++j){
                VT gauche_px = save_img.at<Vec3b>(i, j+1);
                VT bas_px = save_img.at<Vec3b>(i+1,j);
                VT px = save_img.at<Vec3b>(i, j);

                int contrasteX{gauche_px[0]-px[0]+gauche_px[1]-px[1]+gauche_px[2]-px[2]};
                int contrasteY{bas_px[0]-px[0]+bas_px[1]-px[1]+bas_px[2]-px[2]};
                //(next_px[0]-px[0])+(next_px[1]-px[1])+(next_px[2]-px[2]))

                if(contrasteX>128)//392 = 3 X 128, utilisé pour faire la moyenne sans diviser.
                    resX+=j;
                if(contrasteY>128)//392 = 3 X 128, utilisé pour faire la moyenne sans diviser.
                    resY+=i;
		//std::cout<<"cH:"<<contrasteX<<" cV:"<<contrasteY<<"\n"<<std::flush;

            }
        }
	//std::cout<<"resrH:"<<resX<<" resV:"<<resY<<"\n"<<std::flush;
        int diffX = resX - ancienneVariationX;
        int diffY = resY - ancienneVariationY;

        ancienneVariationX = resX;
        ancienneVariationY = resY;

        return MDData(diffX, diffY);
    }

    int camp3Capture::variationVerticale(Mat& m){
        It img = save_img.begin<VT>(), img_end = save_img.end<VT>(), img_next = img, contour = m.begin<VT>(), img_begin=img;
        img_next+=save_img.rows;
        for(int i = 0; i<save_img.cols;++i){
            for(int j = 0; j<save_img.rows&&img_next!=img_end;++j){
                *contour = *img_next - *img;
                img+=save_img.cols;
                img_next+=save_img.cols;
                contour+=save_img.cols;
            }
            img = img_begin;
            img+=i;
            contour = m.begin<VT>();
            contour+=i;
            img_next=img;img_next++;
        }
    }


    void camp3Capture::gradient(Mat& m){
    }

    const Mat* camp3Capture::getMat(){
        return &save_img;
    }

    long long camp3Capture::determinerIndice(){
        long long res;
        It img = contours.begin<VT>();
          for(int i = 0; i<save_img.rows;++i){
            for(int j = 0; j<save_img.cols;++j){
                VT v{*img};

                if((v[0]+v[1]+v[2])>392)
                    res+=j;
            }
        }
        return res;
    }

   int camp3Capture::determinerIndice(Mat m){
        int res=0;
        int cols=0;

          for(int i = 0; i<m.rows;++i){
            for(int j = 0; j<m.cols;++j){
                VT v = m.at<Vec3b>(i, j);

                if((v[0]+v[1]+v[2])>0)
                    res+=j;
                ++cols;

            }
        }
        return res;
    }

    void camp3Capture::saveAsPPM(string name, const Mat* m){
         std::vector<int> params(2);
         params[0] = CV_IMWRITE_PXM_BINARY;
         params[1] = 0;
         imwrite(name, *m, params);
    }


    void camp3Capture::capture(){
        cap >> save_img;
    }

    void camp3Capture::lisser(){

        for(int i = 1; i<save_img.rows-1;++i){
            for(int j = 1; j<save_img.cols-1;++j){
                VT v = save_img.at<Vec3b>(i, j);
                VT v_haut_gauche = save_img.at<Vec3b>(i-1, j-1);
                VT v_haut_centre = save_img.at<Vec3b>(i-1, j);
                VT v_haut_droit = save_img.at<Vec3b>(i-1, j+1);
                VT v_centre_gauche = save_img.at<Vec3b>(i, j-1);
                VT v_centre_droite = save_img.at<Vec3b>(i, j+1);
                VT v_bas_gauche = save_img.at<Vec3b>(i+1, j-1);
                VT v_bas_centre = save_img.at<Vec3b>(i+1, j);
                VT v_bas_droite = save_img.at<Vec3b>(i+1, j+1);

                uchar R = (v_haut_gauche[0]+v_haut_centre[0]+v_haut_droit[0]+v_centre_gauche[0]+v_centre_droite[0]+v_bas_gauche[0]+v_bas_centre[0]+v_bas_droite[0])/8;
                uchar G = (v_haut_gauche[1]+v_haut_centre[1]+v_haut_droit[1]+v_centre_gauche[1]+v_centre_droite[1]+v_bas_gauche[1]+v_bas_centre[1]+v_bas_droite[1])/8;
                uchar B = (v_haut_gauche[2]+v_haut_centre[2]+v_haut_droit[2]+v_centre_gauche[2]+v_centre_droite[2]+v_bas_gauche[2]+v_bas_centre[2]+v_bas_droite[2])/8;

                VT v_lisse{R, G, B};

                save_img.at<Vec3b>(i, j) = v_lisse;

            }
        }
    }
