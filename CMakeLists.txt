cmake_minimum_required(VERSION 2.8)

project( CAMP3 )

find_package( OpenCV REQUIRED )


file(GLOB CAMP3_CPP "${CAMP3_SOURCE_DIR}/*.cpp")
file(GLOB CAMP3_H "${IFT630_TP2_SOURCE_DIR}/*.h")

add_executable(camp3 ${CAMP3_CPP} ${CAMP3_H})

target_link_libraries( camp3
    ${OpenCV_LIBS}
    libfmodex64-4.44.30.so
)

add_definitions("-std=c++0x")
