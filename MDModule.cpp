//Created by : Francois Genois
#include "MDModule.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <iostream>

using namespace std;
using size_type = MDModule::size_type;

MDModule::MDModule(std::shared_ptr<AudioPlayer> _audioPlayer,
                    size_type nbGroup,
                    size_type sizeGroup,
                    int millisecondsDeltaCommands)
    : threadModule{nullptr}
    , bufferElementCount{0}
    , thingsToDo{0}
    , groupSize{sizeGroup}
    , audioPlayer{_audioPlayer}
    , mutexBuffer{}
    , begin{0}
    , end{0}
    , lastCommandSent{}
    , deltaTimeCommands{millisecondsDeltaCommands}
    , bufferMDData(sizeGroup+nbGroup+1, MDData())
{}

void MDModule::Init()
{
    audioPlayer->Init();
    audioPlayer->Play();
    Abort();
    continueExecution = true;
    threadModule = new thread(&MDModule::ThreadMDModule, this);
}

void MDModule::Abort()
{
    if (threadModule)
    {
        continueExecution = false;
        threadModule->join();
    }

    delete threadModule;
    threadModule = nullptr;
}

size_type MDModule::GetNextIndex(const size_type currentIndex, const size_type increment)
{
    return (currentIndex + increment) % bufferMDData.size();
}

size_type MDModule::GetPreviousIndex(const size_type currentIndex, const size_type decrement)
{
    return bufferMDData.size() - 1 - ((bufferMDData.size() - 1 - currentIndex + decrement) % bufferMDData.size());
}

void MDModule::AddMDData(const MDData& data)
{
    //cout << "add data" << data.xValue << "::" << data.yValue << '\n';
    mutexBuffer.lock();
    if(GetNextIndex(begin) != end)
    {
        ++bufferElementCount;
        bufferMDData[begin] = data;
        begin = GetNextIndex(begin);
    }
    else
        cout << "buffer full\n";
    mutexBuffer.unlock();
    thingsToDo.Up();
}

bool MDModule::GetMDDataGroup(MDData* outData, const int size)
{
    for (int i = 0, index = end; i != size; ++i)
    {
        if (index == begin)
            return false;

        outData[i] = bufferMDData[index];
        index = GetNextIndex(index);
    }

    return true;
}

void MDModule::RemoveLastElement()
{
    mutexBuffer.lock();
    if (GetNextIndex(end) != begin)
        end = GetNextIndex(end);
    mutexBuffer.unlock();
}

MDData::MovementStrenght MDModule::GetXMovement(const MDData* data, const int size) const
{
    if (std::all_of( data + 1
                        , data + size
                        , [&data](MDData i) { return i.GetHorizontalMovement() == data[0].GetHorizontalMovement();}))
        return data[0].GetHorizontalMovement();
    return MDData::NONE;
}

MDData::MovementStrenght MDModule::GetYMovement(const MDData* data, const int size) const
{
    if (std::all_of( data + 1
                        , data + size
                        , [&data](MDData i) { return i.GetVerticalMovement() == data[0].GetVerticalMovement();}))
        return data[0].GetVerticalMovement();
    return MDData::NONE;
}

void MDModule::ThreadMDModule()
{
    MDData data[groupSize];

    while (continueExecution)
    {
        thingsToDo.Down();
        if(GetMDDataGroup(data, groupSize))
        {
            RemoveLastElement();
            auto yMov = GetYMovement(data, groupSize);
            auto xMov = GetXMovement(data, groupSize);

            if(chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now() - lastCommandSent).count() > deltaTimeCommands)
            {
                switch (yMov)
                {
                    case MDData::NEGATIVE :
                        cout << "top -> Increment volume\n";
                        audioPlayer->QueueCommand(AudioPlayer::IncrementVolumeCmd);
                        break;
                    case MDData::POSITIVE :
                        cout << "down -> Decrement volume\n";
                        audioPlayer->QueueCommand(AudioPlayer::DecrementVolumeCmd);
                        break;
                    case MDData::NONE : break;
                }
                switch (xMov)
                {
                    case MDData::NEGATIVE :
                        cout << "right -> Next Song" << endl;
                        audioPlayer->QueueCommand(AudioPlayer::NextSongCmd);
                        break;
                    case MDData::POSITIVE :
                        cout << "left -> Previous Song" << endl;
                        audioPlayer->QueueCommand(AudioPlayer::PreviousSongCmd);
                        break;
                    case MDData::NONE : break;
                }
                if (xMov != MDData::NONE || yMov != MDData::NONE)
                    lastCommandSent = chrono::system_clock::now();
            }
        } else {}
    }
}