//Created by : Francois Genois
#pragma once
#include <mutex>
#include <condition_variable>

//Source : http://stackoverflow.com/questions/4792449/c0x-has-no-semaphores-how-to-synchronize-threads
class Semaphore{
private:
    std::mutex mtx;
    std::condition_variable cv;
    int count;

public:
    Semaphore(int count_ = 0):count(count_){;}

    void Up();
    void Down();
};