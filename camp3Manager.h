//Created by : Simon Gilbert
//Modified by : François Genois

#ifndef CAMP3MANAGER_H
#define CAMP3MANAGER_H

#include "camp3Capture.h"

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <iostream>
#include <stdio.h>
#include <vector>
#include <utility>
#include <string>
#include <chrono>


#include "MDModule.h"
#include "MDData.h"
#include "CaMP3CaptureV2.h"

using std::pair;
using std::string;
using cv::Mat;

typedef std::chrono::duration<int,std::micro>microseconds_type;

class camp3Manager{

private:
    const int fps;
    CaMP3CaptureV2 captureHelper;
    MDModule m;
    std::mutex cv_m;
    std::condition_variable cv;

public:
    camp3Manager(int fps_);
    void detecte();
};

#endif