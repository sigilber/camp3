#ifndef ARENAFIXE_H
#define ARENAFIXE_H

#include <new>

class arenaFixe
{
private:
    void *memory[50];
    bool places[50] = {};
    const int FrameSize = 3686404;
    const int nbFrame = 50;

public:
    arenaFixe()
    {
        for(int i = 0; i<nbFrame;++i)
            memory[i] = malloc(FrameSize);
    }

    ~arenaFixe()
    {
        for(int i=0;i<nbFrame;++i)
            free (memory[i]);
    }

    void* allouer()
    {
        for(int i=0;i<nbFrame;++i)
            if(!places[i])
            {
                places[i] = true;
                return memory[i];
            }
        return 0;
    }

    void liberer(void* p)
    {
        for(int i=0;i<nbFrame;++i)
            if(memory[i]==p)
            {
                places[i] = 0;
                return;
            }
    }
};

#endif