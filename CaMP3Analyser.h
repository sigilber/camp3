//Created by : François Genois
#pragma once
#include <opencv2/video/tracking.hpp>

#include "MDData.h"

class CaMP3Analyser
{
    const double MAX_FEATURE_COUNT = 25;
    const double QUALITY_LEVEL = 0.01;
    const int MIN_DIST = 5;

    const int NbImageReset = 15;
    int countResetTracking = 0;

    cv::Mat prevImage, nextImage;
    cv::Mat prevFeatures, nextFeatures;
    void ResetTrackingPoints();

public:
    CaMP3Analyser(cv::Mat firstImage);
    void Init();
    MDData AnalyseNextImage(cv::Mat nextImage);
};