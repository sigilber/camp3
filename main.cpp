//Created by : Simon Gilbert
//Modified by : Simon Gilbert, Francois Genois
#include "camp3Capture.h"
#include <iostream>
#include <sstream>
#include <thread>
#include "camp3Manager.h"
#include "AudioPlayer.h"

using std::thread;

int main(int argc, char* argv[])
{
    int fps = 15;
    if(argc > 1)
    {
        std::stringstream ss;
        ss << argv[1];
        ss >> fps;
    }

    camp3Manager manager{ fps };
    manager.detecte();
}
