//Created by : Francois Genois
#include "MDData.h"

MDData::MDData()
    : xValue{}
    , yValue{}
{}

MDData::MDData(precision_type _xValue, precision_type _yValue)
    : xValue(_xValue)
    , yValue(_yValue)
{}

MDData::MovementStrenght MDData::GetMovementStrenght(
    MDData::precision_type thresholdLow,
    MDData::precision_type thresholdHi,
    MDData::precision_type value) const
{
    if (thresholdLow <= value && thresholdHi >= value)
        return MDData::POSITIVE;
    else if (-thresholdHi <= value && -thresholdLow >= value)
        return MDData::NEGATIVE;
    return MDData::NONE;
}

MDData::MovementStrenght MDData::GetHorizontalMovement() const
{
    return GetMovementStrenght(thresholdLowX, thresholdHiX, xValue);
}

MDData::MovementStrenght MDData::GetVerticalMovement() const
{
    return GetMovementStrenght(thresholdLowY, thresholdHiY, yValue);
}