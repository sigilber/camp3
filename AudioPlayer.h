//Created by : Francois Genois
#pragma once
#include "fmodex/fmod.hpp"
#include "Semaphore.h"
#include <vector>
#include <string>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <chrono>

using std::mutex;
using std::condition_variable;
typedef std::chrono::duration<int,std::micro>microseconds_type;

class AudioPlayer
{
public:
    using index_type = std::vector<std::string>::size_type;
    using size_type = std::vector<std::string>::size_type;

    enum CommandType { IncrementVolumeCmd, DecrementVolumeCmd, NextSongCmd, PreviousSongCmd, NoneCmd };
    bool QueueCommand(CommandType cmdType);
    void ExecuteCommand(CommandType cmdType);
private:
    std::mutex mutexBuffer;
    std::vector<CommandType> commmandQueue;

    std::vector<std::string> soundFiles;
    index_type currentFileIndex;

    bool continueExecution;
    std::thread* updateThread;
    std::mutex cv_m;
    std::condition_variable cv;

    std::thread* executeCommandsThread;
    Semaphore thingsToDo;

    float currentVolume;
    static constexpr float MAX_VOLUME = 1.0f;
    static constexpr float MIN_VOLUME = 0.0f;
    FMOD::System *system;
    FMOD::Sound *currentSound;
    FMOD::Channel *currentChannel;
    bool FirstPlay();

    void UpdateThread();
    void ExecuteCommandThread();
    void ExecuteCommand();

    index_type GetNextSongID() const;
    index_type GetPreviousSongID() const;

    void OnSoundEnd();
    static FMOD_RESULT OnSoundEndTrampoline(FMOD_CHANNEL *, FMOD_CHANNEL_CALLBACKTYPE, void *, void *);

public:
    AudioPlayer(int commandQueueSize = 10);
    ~AudioPlayer();
    void Init();
    void Abort();

    void AddFiles(std::string audioFolder);
    index_type AddFile(std::string audioFile);
    bool RemoveFile(index_type index);
    std::string GetFileAtIndex(index_type index) const;
    size_type GetCount() const;
    bool SetCurrentFile(index_type index, bool startPlay = true);

    bool Play();
    void Pause();
    void Resume();
    void Stop();
    void PlayNextSong();
    void PlayPreviousSong();
    void Restart();

    void SetVolume(float v);
    void IncrementVolume(float v = 0.1F);
    void DecrementVolume(float v = 0.1F);
};