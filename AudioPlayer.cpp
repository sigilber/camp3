//Created by : Francois Genois
#include <algorithm>
#include <dirent.h>
#include <assert.h>
#include <iostream>

#include "AudioPlayer.h"

using std::string;
using std::max;
using std::min;
using std::thread;
using index_type = AudioPlayer::index_type;
using namespace std::chrono;

#ifndef RELEASE
void ERRCHECK(FMOD_RESULT result)
{
    static int i=0;
    i++;
    if (result != FMOD_OK)
    {
        std::cout << "FMOD error! " << i << std::endl;
    }
}
#else
void ERRCHECK(FMOD_RESULT result){}
#endif

AudioPlayer::AudioPlayer(int commandQueueSize)
    : updateThread{}
    , executeCommandsThread{}
    , currentSound{}
    , currentChannel{}
    , system{}
    , currentVolume(1.0f)
    , continueExecution(false)
    , currentFileIndex(0)
    , mutexBuffer{}
    , thingsToDo{0}
{
    std::cout << "size" << commmandQueue.size();
    commmandQueue.reserve(commandQueueSize);
    std::cout << "size" << commmandQueue.size();
    ERRCHECK(FMOD::System_Create(&system));
    ERRCHECK(system->init(32, FMOD_INIT_NORMAL, 0));
}

AudioPlayer::~AudioPlayer()
{
    Abort();
    if (currentSound)
        ERRCHECK(currentSound->release());
    ERRCHECK(system->release());
}

void AudioPlayer::AddFiles(std::string audioFolder)
{
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (audioFolder.c_str())) != NULL)
    {
        while ((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0] != '.')
            {
                soundFiles.push_back(audioFolder + ent->d_name);
                std::cout << audioFolder + ent->d_name << std::endl;
            }
        }
        closedir (dir);
    }
    else
        perror ("Could not open directory.");
}

bool AudioPlayer::QueueCommand(CommandType cmdType)
{
    mutexBuffer.lock();
    if (commmandQueue.size() < commmandQueue.capacity())
    {
        commmandQueue.push_back(cmdType);
    }
    mutexBuffer.unlock();
    thingsToDo.Up();
}

void AudioPlayer::ExecuteCommand(CommandType cmdType)
{
    switch (cmdType)
    {
        case IncrementVolumeCmd: IncrementVolume(); break;
        case DecrementVolumeCmd: DecrementVolume(); break;
        case NextSongCmd: PlayNextSong(); break;
        case PreviousSongCmd: PlayPreviousSong(); break;
        case NoneCmd :
        default: break;
    }
}

index_type AudioPlayer::AddFile(std::string audioFile)
{
    soundFiles.push_back(audioFile);
    return soundFiles.size() - 1;
}

bool AudioPlayer::RemoveFile(index_type idSong)
{
    if (idSong >= soundFiles.size())
        return false;

    soundFiles.erase(soundFiles.begin() + idSong);

    if (currentFileIndex < idSong)
        --currentFileIndex;

    return true;
}

bool AudioPlayer::SetCurrentFile(index_type index, bool startPlay)
{
    if (index >= soundFiles.size())
        return false;

    currentFileIndex = index;
    if(!startPlay)
        return true;

    return Play();
}

std::string AudioPlayer::GetFileAtIndex(index_type index) const
{
    return soundFiles[index];
}

AudioPlayer::size_type AudioPlayer::GetCount() const
{
    return soundFiles.size();
}

bool AudioPlayer::Play()
{
    if (currentFileIndex >= soundFiles.size())
        return false;

    Stop();

    ERRCHECK(system->createSound(soundFiles[currentFileIndex].c_str(), FMOD_SOFTWARE, 0, &currentSound));
    assert(currentSound);
    ERRCHECK(currentSound->setMode(FMOD_LOOP_OFF));

    ERRCHECK(system->playSound(FMOD_CHANNEL_FREE, currentSound, false, &currentChannel));
    assert(currentChannel);
    currentChannel->setUserData((void*)this);
    currentChannel->setCallback(AudioPlayer::OnSoundEndTrampoline);
    ERRCHECK(currentChannel->setVolume(currentVolume));

    return true;
}

void AudioPlayer::Pause()
{
    assert(currentChannel && currentSound);
    ERRCHECK(currentChannel->setPaused(true));
}

void AudioPlayer::Resume()
{
    assert(currentChannel && currentSound);
    ERRCHECK(currentChannel->setPaused(false));
}

void AudioPlayer::Stop()
{
    if (currentChannel)
    {
        currentChannel->setUserData(nullptr);
        currentChannel->stop();
    }

    if (currentSound)
        currentSound->release();

    currentChannel = nullptr;
    currentSound = nullptr;
}

void AudioPlayer::SetVolume(float v)
{
    currentChannel->setVolume((v > MAX_VOLUME) ? MAX_VOLUME : (v < MIN_VOLUME) ? MIN_VOLUME : v);
}

void AudioPlayer::IncrementVolume(float v)
{
    SetVolume(min(currentVolume + v, 1.0f));
}

void AudioPlayer::DecrementVolume(float v)
{
    SetVolume(max(currentVolume + v, 0.0f));
}

void AudioPlayer::UpdateThread()
{
    microseconds_type dura{ 1000000 };
    std::unique_lock<mutex> lock(cv_m);
    while(continueExecution)
    {
        auto now = std::chrono::high_resolution_clock::now();
        std::cout << "update\n";
        system->update();
        cv.wait_until(lock, now + dura);
    }
}

void AudioPlayer::ExecuteCommandThread()
{
    while(continueExecution)
    {
        thingsToDo.Down();
        ExecuteCommand();
    }
}

void AudioPlayer::ExecuteCommand()
{
    mutexBuffer.lock();

    auto hasCommand = !commmandQueue.empty();
    auto command = (hasCommand) ? commmandQueue.back() : NoneCmd;
    if(hasCommand)
        commmandQueue.pop_back();

    mutexBuffer.unlock();

    if(hasCommand)
        ExecuteCommand(command);
}

void AudioPlayer::Init()
{
    assert(system);
    Abort();
    continueExecution = true;
    updateThread = new thread(&AudioPlayer::UpdateThread, this);
    executeCommandsThread = new thread(&AudioPlayer::ExecuteCommandThread, this);
}

void AudioPlayer::Abort()
{
    if (updateThread)
    {
        continueExecution = false;
        updateThread->join();
    }

    delete updateThread;
    updateThread = nullptr;
}

index_type AudioPlayer::GetNextSongID() const
{
    return (currentFileIndex + 1) % soundFiles.size();
}

index_type AudioPlayer::GetPreviousSongID() const
{
    return soundFiles.size() - 1 - ((soundFiles.size() - currentFileIndex) % soundFiles.size());
}

void AudioPlayer::PlayNextSong()
{
    Stop();
    currentFileIndex = GetNextSongID();
    Play();
}

void AudioPlayer::PlayPreviousSong()
{
    Stop();
    currentFileIndex = GetPreviousSongID();
    Play();
}

void AudioPlayer::Restart()
{
    Stop();
    Play();
}

void AudioPlayer::OnSoundEnd()
{
    QueueCommand(NextSongCmd);
}

FMOD_RESULT AudioPlayer::OnSoundEndTrampoline(FMOD_CHANNEL *channel, FMOD_CHANNEL_CALLBACKTYPE type, void *commanddata1, void *commanddata2)
{
    void* userData;
    FMOD_Channel_GetUserData(channel, &userData);

    if (userData)
    {
        AudioPlayer* player = (AudioPlayer*) userData;
        player->OnSoundEnd();
    }

    return FMOD_OK;
}