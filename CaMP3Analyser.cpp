//Created by : François Genois
#include <iostream>
#include "CaMP3Analyser.h"

using cv::Mat;
using cv::goodFeaturesToTrack;

#include "debug.h"

CaMP3Analyser::CaMP3Analyser(Mat firstImage)
    : prevImage{}, nextImage{ firstImage.clone() }
{}

void CaMP3Analyser::Init()
{
    ResetTrackingPoints();
}

void CaMP3Analyser::ResetTrackingPoints()
{
    goodFeaturesToTrack(nextImage,          // the image
                        nextFeatures,        // the output detected features
                        MAX_FEATURE_COUNT,  // the maximum number of features
                        QUALITY_LEVEL,      // quality level
                        MIN_DIST            // min distance between two features
                        );
}

MDData CaMP3Analyser::AnalyseNextImage(Mat newImage)
{
    ++countResetTracking;
    if(countResetTracking >= NbImageReset)
        ResetTrackingPoints();

    prevImage = nextImage.clone();
    prevFeatures = nextFeatures.clone();
    nextImage = newImage.clone();  // Get next image

    // Find the new position of the features
    cv:Mat nstatus, nerr;
    cv::calcOpticalFlowPyrLK(prevImage, nextImage, prevFeatures, nextFeatures, nstatus, nerr);

    //Find the delta of all points
    auto delta = cv::sum(nextFeatures - prevFeatures);
    return MDData(delta[0], delta[1]);
}
