//Created by : Simon Gilbert
#ifndef CAMP3CAPTURE_H
#define CAMP3CAPTURE_H


#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <iostream>
#include <stdio.h>
#include <vector>
#include <utility>
#include <string>

#include "MDData.h"


//using namespace std;
using cv::Mat;
using cv::Vec;
using cv::VideoCapture;
using cv::MatIterator_;
using std::pair;
using std::string;

using namespace cv;

class MDData;

typedef Vec<uchar, 3> VT;
typedef Vec<char, 3> VTS;
typedef MatIterator_<VT> It ;

class camp3Exception{};

class camp3Capture{

public:
     Mat save_img; //a enlever! on doit faire l'acquisition de l'espace mémoire au démarrage du programme.
     VideoCapture cap;///voir si on peut mettre la variable cap static
     Mat contours;
     int ancienneVariationX, ancienneVariationY;


    camp3Capture();
    camp3Capture(Mat);
    camp3Capture(string);
    camp3Capture(VideoCapture);
    ~camp3Capture();

    const Mat* detecterContours();
    long long determinerIndice();
    int determinerIndice(Mat m);
    void saveAsPPM(string, const Mat*);
    const Mat* getMat();
    int variationHorizontale(Mat&);
    int variationVerticale(Mat&);
    MDData variationBidirectionnelle();
    void contourHorizontal(Mat&);
    void contourVertical(Mat&);
    void gradient(Mat&);
    void lisser();

    void capture();

};

#endif

