//Created by : François Genois
#include "CaMP3CaptureV2.h"

CaMP3CaptureV2::CaMP3CaptureV2()
    : cap{0}
{}

CaMP3CaptureV2::~CaMP3CaptureV2()
{
    cap.release();
}

void CaMP3CaptureV2::GetImage(cv::Mat& image)
{
    cv::Mat floatMat;

    cap >> image;
    cv::cvtColor(image, floatMat, CV_RGB2GRAY);
    floatMat.convertTo(image, CV_8UC1);
}