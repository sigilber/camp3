//Created by : François Genois
#pragma once

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

class CaMP3CaptureV2
{
    cv::VideoCapture cap;

public:
    CaMP3CaptureV2();
    ~CaMP3CaptureV2();
    void GetImage(cv::Mat& image);
};